export type ScaleFactorType = 0 | 0.5 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

const SCALE = 8;

/**
 * @param scale factor 0 | 0.5 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
 * @returns scaled sizing
 */

export const getSize = (param: ScaleFactorType) => {
  return param * SCALE;
};
