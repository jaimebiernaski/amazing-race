import { FC, memo } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { getSize } from 'utils/sizeUtil';

import { RaceStatus } from '../types';

export const CurrentRaceStatus: FC<{
  currentStatus: RaceStatus;
}> = memo(({ currentStatus }) => {
  const _getRacerStatus = () => {
    switch (currentStatus) {
      case RaceStatus.FINISHED:
        return 'Finished!';
      case RaceStatus.IN_PROGRESS:
        return 'Running...';
      default:
        return 'Ready!';
    }
  };
  return (
    <View style={styles.container}>
      <Text style={styles.statusText}>{_getRacerStatus()}</Text>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    padding: getSize(4),
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  statusText: {
    fontSize: getSize(3),
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
