import { FC, memo } from 'react';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';

import { getSize } from 'utils/sizeUtil';

import CONST from '../constants';
import { RacerType, RaceStatus } from '../types';

export const ListItem: FC<{
  index: number;
  racer: RacerType;
  raceStatus: RaceStatus;
}> = memo(({ racer, raceStatus }) => {
  const _renderRacerStatus = () => {
    if (racer.likelihood === -1 && RaceStatus.IN_PROGRESS !== raceStatus) {
      return <Text>--</Text>;
    }
    if (racer.likelihood !== CONST.INITAL_RACER_TIME) {
      return <Text>{(racer.likelihood! * 100).toFixed(2)}%</Text>;
    }
    return <ActivityIndicator />;
  };

  return (
    <View style={styles.container}>
      <View style={styles.nameContainer}>
        <Text style={styles.name}>{racer.name}</Text>
      </View>
      <View style={styles.statusContainer}>
        <View>{_renderRacerStatus()}</View>
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    overflow: 'hidden',
    padding: getSize(2),
  },
  name: {
    fontSize: getSize(2),
  },
  nameContainer: {
    width: '75%',
  },
  statusContainer: {
    width: '25%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
