import { FC, memo, useEffect, useMemo, useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';

import { generateRacerWinLikelihoodCalculator } from 'utils/raceUtil';
import { getSize } from 'utils/sizeUtil';

import { Button } from './Button';
import { CurrentRaceStatus } from './CurrentRaceStatus';
import { ListItem } from './ListItem';
import CONST from '../constants';
import { RacerType, RaceStatus } from '../types';

export const Race: FC<{
  racers: RacerType[];
}> = memo(({ racers }) => {
  const [raceStatus, setRaceStatus] = useState(RaceStatus.NOT_STARTED);

  const [results, setResults] = useState<RacerType[]>([]);

  const racerWithTime = useMemo(
    () =>
      racers.map((racer) => ({
        ...racer,
        likelihood: CONST.INITAL_RACER_TIME,
      })),
    [racers]
  );

  const _startRace = (racer: RacerType) =>
    new Promise<void>((resolve) => {
      generateRacerWinLikelihoodCalculator()((likelihood: number) => {
        setResults((prev) =>
          [...prev, { name: racer.name, likelihood }].sort(
            (a, b) => b.likelihood! - a.likelihood!
          )
        );
        resolve(); // resolve the promise
      });
    });

  const _handleStartRace = async () => {
    setRaceStatus(RaceStatus.IN_PROGRESS);
    await Promise.all(racers.map(_startRace)); // wait for all promises to resolve. Starts all simultaneously with Promise.all
  };

  const _handleResetRace = () => {
    setRaceStatus(RaceStatus.NOT_STARTED);
    setResults([]);
  };

  useEffect(() => {
    if (results.length === racers.length) {
      setRaceStatus(RaceStatus.FINISHED);
    }
  }, [results.length, racers.length]);

  return (
    <View style={styles.container}>
      <FlatList
        data={[
          ...results,
          ...racerWithTime.filter(
            (racer) => !results.some((r) => r.name === racer.name)
          ),
        ]}
        renderItem={(item) => (
          <ListItem
            racer={item.item}
            raceStatus={raceStatus}
            index={item.index}
          />
        )}
        contentContainerStyle={styles.contentContainerStyle}
      />
      <CurrentRaceStatus currentStatus={raceStatus} />
      <View style={styles.bottomContainer}>
        {raceStatus !== RaceStatus.FINISHED &&
          raceStatus !== RaceStatus.IN_PROGRESS && (
            <Button title="START" onPress={_handleStartRace} />
          )}

        {raceStatus === RaceStatus.FINISHED && (
          <Button title="RESET" onPress={_handleResetRace} />
        )}
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  bottomContainer: {
    height: getSize(10),
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: getSize(2),
  },
  contentContainerStyle: {
    marginTop: getSize(5),
    margin: getSize(2),
    padding: getSize(2),
    borderRadius: getSize(2),
    backgroundColor: 'silver',
  },
});
