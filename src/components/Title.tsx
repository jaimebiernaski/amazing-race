import { memo } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { getSize } from 'utils/sizeUtil';

export const Title = memo(() => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>AMAZING RACE</Text>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    marginTop: getSize(4),
    padding: getSize(1),
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: getSize(4),
    fontWeight: 'bold',
  },
});
