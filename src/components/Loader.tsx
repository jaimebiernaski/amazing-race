import { FC, memo, useCallback, useState } from 'react';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';

import axios from 'axios';
import { StatusBar } from 'expo-status-bar';

import { getSize } from 'utils/sizeUtil';

import { Button } from './Button';
import CONST from '../constants';
import { RacersType } from '../types';

export const Loader: FC<{
  handleDataLoad: (data: RacersType) => void;
}> = memo(({ handleDataLoad }) => {
  const [loading, setLoading] = useState(false);

  const _handleLoadDataPress = useCallback(async () => {
    try {
      setLoading(true);
      const response = await axios.get(CONST.API_URL);
      setLoading(false);
      handleDataLoad(response.data);
    } catch (err) {
      setLoading(false);
      throw new Error(err);
    }
  }, [handleDataLoad]);

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Text style={styles.subtitle}>Welcome to</Text>
      <Text style={styles.title}> The Amazing Race!</Text>
      <Text style={styles.info}>Load racers to continue...</Text>
      <View style={styles.bottomContainer}>
        {loading ? (
          <ActivityIndicator size={getSize(5)} />
        ) : (
          <>
            <Button title="Load Racers" onPress={_handleLoadDataPress} />
          </>
        )}
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  title: {
    fontSize: getSize(4),
  },
  subtitle: {
    fontSize: getSize(3),
  },
  info: {
    fontSize: getSize(2),
    color: 'gray',
  },
  bottomContainer: {
    height: getSize(10),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
