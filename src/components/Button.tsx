import { FC, memo } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

import { getSize } from 'utils/sizeUtil';

export const Button: FC<{ title: string; onPress: () => void }> = memo(
  ({ title, onPress }) => {
    return (
      <TouchableOpacity onPress={onPress} style={styles.container}>
        <Text style={styles.title}>{title}</Text>
      </TouchableOpacity>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    minWidth: getSize(5) * 5,
    padding: getSize(1),
    borderRadius: getSize(2),
    borderColor: '#000',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: getSize(3),
    padding: getSize(1),
  },
});
