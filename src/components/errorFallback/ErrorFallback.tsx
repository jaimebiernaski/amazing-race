import { FC, memo } from 'react';
import { ScrollView, Text } from 'react-native';

import { ErrorFallbackProps } from './ErrorFallback.types';
import { getSize } from '../../utils/sizeUtil';

export const ErrorFallback: FC<ErrorFallbackProps> = memo(({ message }) => {
  return (
    <ScrollView
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: getSize(5),
      }}
    >
      <Text style={{ fontSize: getSize(4), padding: getSize(2) }}>
        Oppsss... ERROR!
      </Text>
      <Text>{message}</Text>
    </ScrollView>
  );
});
