export type RacerType = {
  name: string;
  length?: number;
  color?: string;
  weight?: number;
  likelihood?: number;
};

export type RacersType = {
  racers: RacerType[];
};

export enum RaceStatus {
  NOT_STARTED = 'NOT_STARTED',
  IN_PROGRESS = 'IN_PROGRESS',
  FINISHED = 'FINISHED',
}
