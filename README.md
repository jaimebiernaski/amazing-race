# Amazing Race

The project was built with Expo and Typescript.

## Getting started

For running the app, run:

```
$ yarn install
$ yarn ios
```

**Note**: It was tested only in iOS simulator.

## TODO

The next steps would be:

- Add test
- Improve UI
