import { useState } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';

import { StatusBar } from 'expo-status-bar';
import { ErrorBoundary } from 'react-error-boundary';

import { Race } from 'components/Race';
import { Title } from 'components/Title';
import { getSize } from 'utils/sizeUtil';

import { ErrorFallback } from './src/components/errorFallback/ErrorFallback';
import { Loader } from './src/components/Loader';
import { RacersType } from './src/types';

export default function App() {
  const [data, setData] = useState<RacersType>({ racers: [] });

  const _handleLoadData = (data: RacersType) => {
    setData(data);
  };

  return (
    <ErrorBoundary
      FallbackComponent={({ error }) => (
        <ErrorFallback message={error.message} />
      )}
    >
      <StatusBar style="auto" />
      <SafeAreaView style={styles.container}>
        <Title />
        {data.racers.length === 0 ? (
          <Loader handleDataLoad={_handleLoadData} />
        ) : (
          <Race racers={data.racers} />
        )}
      </SafeAreaView>
    </ErrorBoundary>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: getSize(5),
  },
});
